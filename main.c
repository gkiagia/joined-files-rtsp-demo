#include <gst/rtsp-server/rtsp-server.h>
#include <string.h>

GST_PLUGIN_STATIC_DECLARE (coreelements);
GST_PLUGIN_STATIC_DECLARE (multifile);
GST_PLUGIN_STATIC_DECLARE (autoconvert);
GST_PLUGIN_STATIC_DECLARE (rtp);
GST_PLUGIN_STATIC_DECLARE (rtpmanager);
GST_PLUGIN_STATIC_DECLARE (udp);
GST_PLUGIN_STATIC_DECLARE (tcp);
GST_PLUGIN_STATIC_DECLARE (isomp4);
GST_PLUGIN_STATIC_DECLARE (audioparsers);
GST_PLUGIN_STATIC_DECLARE (videoparsersbad);
GST_PLUGIN_STATIC_DECLARE (typefindfunctions);
GST_PLUGIN_STATIC_DECLARE (app);

typedef struct
{
  gchar **filenames;
  GList *parsers;
  GList *payloaders;
  GstElement *bin;
} MediaData;

static void
media_data_free (MediaData *d)
{
  g_list_free_full (d->parsers, g_object_unref);
  g_list_free_full (d->payloaders, g_object_unref);
  g_slice_free (MediaData, d);
}

typedef struct
{
  GList *parse;
  GList *payload;
} FilterData;

static gboolean
payloader_filter (GstPluginFeature * feature, FilterData * data)
{
  const gchar *klass;
  GstElementFactory *fact;
  GList **list = NULL;

  /* we only care about element factories */
  if (G_UNLIKELY (!GST_IS_ELEMENT_FACTORY (feature)))
    return FALSE;

  if (gst_plugin_feature_get_rank (feature) < GST_RANK_MARGINAL)
    return FALSE;

  fact = GST_ELEMENT_FACTORY_CAST (feature);

  klass = gst_element_factory_get_metadata (fact, GST_ELEMENT_METADATA_KLASS);

  if (strstr (klass, "Parser") && strstr (klass, "Codec"))
    list = &data->parse;
  else if (strstr (klass, "Payloader") && strstr (klass, "RTP"))
    list = &data->payload;

  if (list) {
    GST_DEBUG ("adding %s", GST_OBJECT_NAME (fact));
    *list = g_list_prepend (*list, gst_object_ref (fact));
  }

  return FALSE;
}

static void
find_factories (MediaData * d)
{
  FilterData data = { NULL, NULL };

  gst_registry_feature_filter (gst_registry_get (), (GstPluginFeatureFilter)
      payloader_filter, FALSE, &data);
  /* sort */
  d->parsers =
      g_list_sort (data.parse, gst_plugin_feature_rank_compare_func);
  d->payloaders =
      g_list_sort (data.payload, gst_plugin_feature_rank_compare_func);
}

static gchar **
format_location_cb (GstElement * splitmuxsrc, MediaData *d)
{
  g_print ("returning argv for location\n");
  return g_strdupv (d->filenames);
}

static void
pad_added_cb (GstElement * splitmuxsrc, GstPad * pad, MediaData * d)
{
  GstElement *parser;
  GstElement *payloader;
  GstPad *peer;
  GstPad *ghost;

  g_print ("pad %s:%s added\n", GST_DEBUG_PAD_NAME (pad));

  parser = gst_element_factory_make ("autoconvert", NULL);
  payloader = gst_element_factory_make ("autoconvert", NULL);

  g_object_set (parser, "factories",
      g_list_copy_deep (d->parsers, (GCopyFunc) g_object_ref, NULL), NULL);
  g_object_set (payloader, "factories",
      g_list_copy_deep (d->payloaders, (GCopyFunc) g_object_ref, NULL), NULL);

  gst_bin_add_many (GST_BIN (d->bin), parser, payloader, NULL);

  gst_element_sync_state_with_parent (payloader);
  gst_element_sync_state_with_parent (parser);

  gst_element_link (parser, payloader);

  peer = gst_element_get_static_pad (parser, "sink");
  gst_pad_link (pad, peer);
  gst_object_unref (peer);

  peer = gst_element_get_static_pad (payloader, "src");
  ghost = gst_ghost_pad_new (NULL, peer);
  gst_object_unref (peer);
  gst_pad_set_active (ghost, TRUE);
  gst_element_add_pad (d->bin, ghost);
}

static void
no_more_pads_cb (GstElement * splitmuxsrc, MediaData * d)
{
  gst_element_no_more_pads (d->bin);
}

static void
media_configure_cb (GstRTSPMediaFactory * factory, GstRTSPMedia * media,
    gpointer data)
{
  MediaData *d;
  GstElement *topbin;
  GstElement *splitmuxsrc;

  d = g_slice_new0 (MediaData);
  d->filenames = data;
  find_factories (d);
  g_object_set_data_full (G_OBJECT (media), "MediaData", d,
      (GDestroyNotify) media_data_free);

  topbin = gst_rtsp_media_get_element (media);
  d->bin = gst_bin_get_by_name_recurse_up (GST_BIN (topbin), "dynpay0");
  g_object_unref (topbin);

  splitmuxsrc = gst_element_factory_make ("splitmuxsrc", NULL);

  gst_bin_add (GST_BIN (d->bin), splitmuxsrc);

  g_signal_connect (splitmuxsrc, "format-location",
      (GCallback) format_location_cb, d);
  g_signal_connect (splitmuxsrc, "pad-added",
      (GCallback) pad_added_cb, d);
  g_signal_connect (splitmuxsrc, "no-more-pads",
      (GCallback) no_more_pads_cb, d);
}

gint
main (gint argc, gchar *argv[])
{
  GMainLoop *loop;
  GstRTSPServer *server;
  GstRTSPMountPoints *mounts;
  GstRTSPMediaFactory *factory;

  gst_init (&argc, &argv);
  GST_PLUGIN_STATIC_REGISTER (coreelements);
  GST_PLUGIN_STATIC_REGISTER (multifile);
  GST_PLUGIN_STATIC_REGISTER (autoconvert);
  GST_PLUGIN_STATIC_REGISTER (rtp);
  GST_PLUGIN_STATIC_REGISTER (rtpmanager);
  GST_PLUGIN_STATIC_REGISTER (udp);
  GST_PLUGIN_STATIC_REGISTER (tcp);
  GST_PLUGIN_STATIC_REGISTER (isomp4);
  GST_PLUGIN_STATIC_REGISTER (audioparsers);
  GST_PLUGIN_STATIC_REGISTER (videoparsersbad);
  GST_PLUGIN_STATIC_REGISTER (typefindfunctions);
  GST_PLUGIN_STATIC_REGISTER (app);

  loop = g_main_loop_new (NULL, FALSE);

  server = gst_rtsp_server_new ();
  g_object_set (server, "service", "8554", NULL);

  mounts = gst_rtsp_server_get_mount_points (server);
  factory = gst_rtsp_media_factory_new ();
  gst_rtsp_media_factory_set_launch (factory, "( bin name=dynpay0 )");
  //gst_rtsp_media_factory_set_protocols (factory, GST_RTSP_LOWER_TRANS_TCP);
  g_signal_connect (factory, "media-configure", (GCallback) media_configure_cb,
      (gpointer) &argv[1]);
  gst_rtsp_mount_points_add_factory (mounts, "/demo", factory);
  g_object_unref (mounts);

  gst_rtsp_server_attach (server, NULL);

  g_print ("stream ready at rtsp://127.0.0.1:8554/demo\n");
  g_main_loop_run (loop);

  g_object_unref (server);
  g_main_loop_unref (loop);

  return 0;
}
